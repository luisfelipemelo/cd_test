# TESTE CANOA DIGITAL

Contém um projeto de teste contendo uma lista de "carros", onde o usuário pode listar, visualizar, editar, criar e excluir entradas, além de uma API que executa as mesmas tarefas via JSON.

## Instalação

Basta importar o arquivo de bd e alterar o arquivo /config/app_local.php nas linhas 30 a 35 com os seus dados do mysql.

## Uso

Quanto a interface, ela está pratica, clean e intuitiva, basta clicar nos botões e seguir em frente.

Referente a API ela utiliza os mesmos padrões LARAVEL, podendo ser utilizada das seguintes formas:

- GET /veiculos -
Retorna todos os veículos
- GET /veiculos/{id} -
Retorna os detalhes de um veículo específico

```json
{
    "id": 2,
    "veiculo": "Bliss",
    "marca": "Paradox",
    "ano": 1998,
    "descricao": "Iusto autem ut nulla...",
    "vendido": false,
    "created_at": "2019-12-16T06:27:15+00:00",
    "updated_at": "2020-02-11T14:34:17+00:00"
}
```
- POST /veiculos -
Adiciona um novo veículo, pode ser feito também via URL ex:
[{yourlocalhost}/veiculos.json?veiculo=carro de teste&marca=vrum vrum&ano=2020&descricao=Que carro maravilhoso&vendido=true]()
Sendo que a proteção CSRF foi desativada para possibilitar essa operação em ambiente LOCAL.

```json
{
    "message": "Salvo com sucesso",
    "vehicle": {
        "veiculo": "carro de teste",
        "marca": "vrum vrum",
        "ano": 2020,
        "descricao": "Que carro maravilhoso",
        "vendido": true,
        "created_at": "2020-02-11 20:19:53",
        "updated_at": "2020-02-11 20:19:53",
        "id": 111
    }
}
```
- PUT /veiculos/{id} -
Atualiza os dados de um veículo
```json
{
    "message": "Atualizado com sucesso"
}
```
- DELETE /veiculos/{id}
Apaga o veículo
```json
{
    "message": "Apagado com sucesso"
}
```

## TECNOLOGIAS UTILIZADAS
```bash
CAKEPHP 4.03
PHP 7.3.12
MySQL 8.0
Apache 2.4.41
Faker (gerador de mock data pra semear o bd)
Bootstrap (incluso no CakePHP)
```