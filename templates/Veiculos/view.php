<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Veiculo $vehicle
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Ações') ?></h4>
            <?= $this->Html->link(__('Editar Veículo'), ['action' => 'edit', $vehicle->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Apagar Veículo'), ['action' => 'delete', $vehicle->id], ['confirm' => __('Tem certeza que gostaria de apagar o veículo ID: # {0}?', $vehicle->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Listar Veículos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('Novo Veículo'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="vehicles view content">
            <h3><?= h($vehicle->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Veiculo') ?></th>
                    <td><?= h($vehicle->veiculo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Marca') ?></th>
                    <td><?= h($vehicle->marca) ?></td>
                </tr>
                <tr>
                    <th><?= __('Descrição') ?></th>
                    <td><?= h($vehicle->descricao) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($vehicle->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Ano') ?></th>
                    <td><?= $vehicle->ano ?></td>
                </tr>
                <tr>
                    <th><?= __('Criado Em') ?></th>
                    <td><?= date('d/m/Y H:i', strtotime(h($vehicle->created_at))) ?></td>
                </tr>
                <tr>
                    <th><?= __('Ultima atualização') ?></th>
                    <td><?= date('d/m/Y H:i', strtotime(h($vehicle->updated_at))) ?></td>
                </tr>
                <tr>
                    <th><?= __('Vendido') ?></th>
                    <td><?= $vehicle->vendido ? __('Sim') : __('Não'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
