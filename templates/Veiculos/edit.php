<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Veiculo $vehicle
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Ações') ?></h4>
            <?= $this->Form->postLink(
                __('Apagar Veículo'),
                ['action' => 'delete', $vehicle->id],
                ['confirm' => __('Tem certeza que gostaria de apagar o veículo ID: #{0}?', $vehicle->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('Listar Veículos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="vehicles form content">
            <?= $this->Form->create($vehicle) ?>
            <fieldset>
                <legend><?= __('Editar Veículo') ?></legend>
                <?php
                    echo $this->Form->control('veiculo', ['label' => 'Veículo']);
                    echo $this->Form->control('marca');
                    echo $this->Form->control('ano');
                    echo $this->Form->control('descricao', ['label' => 'Descrição']);
                    echo $this->Form->control('vendido');
//                    echo $this->Form->control('created_at', ['empty' => true]);
//                    echo $this->Form->control('updated_at', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Salvar')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
