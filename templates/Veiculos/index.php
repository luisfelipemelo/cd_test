<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vehicle[]|\Cake\Collection\CollectionInterface $vehicles
 */
?>
<div class="vehicles index content">
    <?= $this->Html->link(__('Novo Veículo'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Veículos') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('Veículo') ?></th>
                    <th><?= $this->Paginator->sort('marca') ?></th>
                    <th><?= $this->Paginator->sort('ano') ?></th>
                    <th><?= $this->Paginator->sort('descricao', 'Descrição') ?></th>
                    <th><?= $this->Paginator->sort('vendido') ?></th>
                    <th><?= $this->Paginator->sort('created_at','Criado em') ?></th>
                    <th><?= $this->Paginator->sort('updated_at', 'Atualizado em') ?></th>
                    <th class="actions"><?= __('Ações') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($vehicles as $vehicle): ?>
                <tr>
                    <td><?= $this->Number->format($vehicle->id) ?></td>
                    <td><?= h($vehicle->veiculo) ?></td>
                    <td><?= h($vehicle->marca) ?></td>
                    <td><?= $vehicle->ano ?></td>
                    <td><?= substr(h($vehicle->descricao), 0 , 15)."..." ?></td>
                    <td><?= h($vehicle->vendido) ? "sim" : "não" ?></td>
                    <td><?= date('d/m/Y H:i', strtotime(h($vehicle->created_at))) ?></td>
                    <td><?= date('d/m/Y H:i', strtotime(h($vehicle->updated_at))) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $vehicle->id]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $vehicle->id]) ?>
                        <?= $this->Form->postLink(__('Apagar'), ['action' => 'delete', $vehicle->id], ['confirm' => __('Tem certeza que gostaria de apagar o veículo ID: # {0}?', $vehicle->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
            <?= $this->Paginator->last(__('ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Página {{page}} de {{pages}}, mostrando {{current}} item(s) do total de {{count}}')) ?></p>
    </div>
</div>
