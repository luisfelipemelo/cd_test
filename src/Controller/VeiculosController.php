<?php

declare(strict_types=1);

namespace App\Controller;

class VeiculosController extends AppController
{
    public function index()
    {
        if($this->request->is('json')){
            $vehicles = $this->Veiculos->find('all');
            $this->set([
                'vehicles' => $vehicles
            ]);
        } else {
            $vehicles = $this->paginate($this->Veiculos);
        }
        $this->set(compact('vehicles'));
    }

    public function view($id = null)
    {
        $vehicle = $this->Veiculos->get($id, [
            'contain' => [],
        ]);

        $this->set('vehicle', $vehicle);
    }

    public function add()
    {
        if ($this->request->is('post')) {
            if($this->request->is('json')) {
                $vehicle = $this->Veiculos->newEntity($this->request->getQuery());
                $sold = $this->request->getQuery('vendido');

                if($sold == "true") {
                    $vehicle->vendido = true;
                } elseif ($sold == "false") {
                    $vehicle->vendido = false;
                } else {
                    $vehicle->vendido = $sold;
                }

                $vehicle->created_at = date_create('now')->format('Y-m-d H:i:s');
                $vehicle->updated_at = date_create('now')->format('Y-m-d H:i:s');
                if ($this->Veiculos->save($vehicle)) {
                    $message = 'Salvo com sucesso';
                } else {
                    $message = 'Houve um erro ao salvar';
                }
                $this->set([
                    'message' => $message,
                    'vehicle' => $vehicle,
                    '_serialize' => ['message', 'vehicle']
                ]);
            } else {
                $vehicle = $this->Veiculos->newEmptyEntity();
                $vehicle = $this->Veiculos->patchEntity($vehicle, $this->request->getData());
                $vehicle->created_at = date_create('now')->format('Y-m-d H:i:s');
                $vehicle->updated_at = date_create('now')->format('Y-m-d H:i:s');
                if ($this->Veiculos->save($vehicle)) {
                    $this->Flash->success(__('O veículo foi salvo com sucesso.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('O veículo não pode ser salvo, por favor, tente novamente.'));
                $this->set(compact('vehicle'));
            }
        }
    }

    public function edit($id = null)
    {
        if ($this->request->is('json')) {
            $vehicle = $this->Veiculos->get($id);
            if ($this->request->is(['post', 'put'])) {
                $vehicle = $this->Veiculos->patchEntity($vehicle, $this->request->getQuery());
                $vehicle->updated_at = date_create('now')->format('Y-m-d H:i:s');
                if ($this->Veiculos->save($vehicle)) {
                    $message = 'Atualizado com sucesso';
                } else {
                    $message = 'Houve um erro ao atualizar';
                }
            }
            $this->set([
                'message' => $message,
                '_serialize' => ['message']
            ]);
        } else {
            $vehicle = $this->Veiculos->get($id, [
                'contain' => [],
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $vehicle = $this->Veiculos->patchEntity($vehicle, $this->request->getData());
                $vehicle->updated_at = date_create('now')->format('Y-m-d H:i:s');
                if ($this->Veiculos->save($vehicle)) {
                    $this->Flash->success(__('O veículo foi salvo com sucesso.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('O veículo não pode ser salvo, por favor, tente novamente.'));
            }
            $this->set(compact('vehicle'));
        }
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vehicle = $this->Veiculos->get($id);
        if ($this->request->is('json')) {
            $message = 'Apagado com sucesso';
            if (!$this->Veiculos->delete($vehicle)) {
                $message = 'Erro ao apagar';
            }
            $this->set([
                'message' => $message,
                '_serialize' => ['message']
            ]);
        } else {
            if ($this->Veiculos->delete($vehicle)) {
                $this->Flash->success(__('O veículo foi apagado.'));
            } else {
                $this->Flash->error(__('O veículo não foi apagado, por favor, tente novamente.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }
}
