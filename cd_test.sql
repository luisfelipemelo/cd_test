-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Tempo de geração: 11-Fev-2020 às 20:39
-- Versão do servidor: 8.0.18
-- versão do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `cd_test`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculos`
--

DROP TABLE IF EXISTS `veiculos`;
CREATE TABLE IF NOT EXISTS `veiculos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `veiculo` varchar(45) NOT NULL,
  `marca` varchar(45) NOT NULL,
  `ano` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `vendido` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `veiculos`
--

INSERT INTO `veiculos` (`id`, `veiculo`, `marca`, `ano`, `descricao`, `vendido`, `created_at`, `updated_at`) VALUES
(1, 'Adventure', 'Nebula', 1998, 'Blanditiis a iustoa ullam sint minus quis et dolores. Minima culpa illo molestiae optio aut. Molestiae ducimus qui harum in aut deserunt qui ut.', 0, '2020-02-11 14:33:37', '2020-02-11 20:31:01'),
(2, 'Bliss', 'Paradox', 1998, 'Iusto autem ut nulla. Expedita laborum voluptatibus sed nobis. Et ut omnis saepe architecto nemo.', 0, '2019-12-16 06:27:15', '2020-02-11 14:34:17'),
(3, 'Whirlpool', 'Specter', 2005, 'Cumque facere occaecati eum non nam nesciunt. Architecto sit assumenda ut deserunt rerum unde corrupti. Eveniet asperiores quos qui.', 0, '2019-03-17 01:31:10', '2020-02-02 14:48:18'),
(4, 'Whirlpool', 'Specter', 1979, 'Nam culpa voluptas voluptatibus soluta esse dolores omnis. Iste quia animi doloremque eos. Voluptas explicabo atque est velit.', 0, '2019-01-05 17:08:05', '2020-01-16 00:29:58'),
(5, 'Passion', 'Conqueror', 1993, 'Aperiam perspiciatis earum quo. Odit quasi inventore ipsa adipisci delectus. Dolores qui mollitia beatae quis earum at exercitationem.', 1, '2019-01-18 04:20:58', '2020-01-14 11:57:22'),
(6, 'Passion', 'Conqueror', 1991, 'Nostrum occaecati et atque asperiores minus. Quibusdam odio enim expedita et. Consequatur nam earum placeat qui nulla.', 0, '2019-07-22 16:17:18', '2020-01-23 23:40:05'),
(7, 'Storm', 'Quicksilver', 1993, 'Voluptas error nam qui delectus distinctio. Fuga molestiae earum velit necessitatibus voluptate autem.', 0, '2019-12-31 20:19:23', '2020-01-01 22:54:59'),
(8, 'Adventure', 'Nebula', 2013, 'Vel in ex omnis voluptate ullam error aut. Ut tempora rerum dolore qui ratione ipsa. Illum voluptate corporis asperiores tempore officiis error.', 0, '2019-01-13 18:28:36', '2020-02-06 15:35:45'),
(9, 'Passion', 'Conqueror', 1987, 'Distinctio reiciendis aut saepe temporibus. Architecto aut eum sed sit. Modi commodi expedita vitae assumenda dolorem minus.', 1, '2019-05-09 02:46:13', '2020-02-07 23:40:38'),
(10, 'Adventure', 'Nebula', 1995, 'In sint in beatae in. Recusandae culpa qui nam sunt fuga aspernatur. Natus facere sunt ad deserunt. Quos incidunt nam magnam.', 1, '2019-03-06 08:11:04', '2020-01-11 03:13:05'),
(11, 'Astral', 'Nebula', 1980, 'Impedit fugiat perspiciatis rerum enim. Nisi debitis repudiandae iste eum. Deserunt illo a laudantium perferendis vel.', 1, '2019-08-06 02:46:59', '2020-02-03 04:38:29'),
(12, 'Reach', 'Quicksilver', 1997, 'Asperiores illum omnis aut omnis deleniti veritatis perferendis dolor. Dolore vitae consequatur officia omnis. Vero perspiciatis et provident a sed.', 0, '2019-05-13 11:58:47', '2020-01-27 06:31:23'),
(13, 'Storm', 'Quicksilver', 2007, 'Quibusdam ipsum eos voluptatum dolorem et. Modi id consectetur rem. Quia est nihil ipsum ipsam velit quis. In dignissimos vel quia voluptatem.', 0, '2019-04-25 00:33:10', '2020-01-18 21:52:35'),
(14, 'Whirlpool', 'Specter', 2014, 'Perferendis illo amet adipisci et labore. Autem dolore perspiciatis debitis blanditiis nobis sapiente dolorum. Occaecati vero minus ut dicta velit.', 0, '2019-02-06 13:18:13', '2020-01-14 17:11:07'),
(15, 'Blaze', 'Paradox', 2011, 'Et quis eaque praesentium non. Et vel eum eligendi nemo hic saepe deleniti.', 1, '2019-04-23 05:05:55', '2020-01-25 00:04:02'),
(16, 'Storm', 'Quicksilver', 2012, 'Ut nemo est fuga aspernatur ullam. Quod error velit aperiam sapiente. In porro qui voluptatem enim.', 0, '2019-04-02 11:35:29', '2020-01-24 15:07:37'),
(17, 'Passion', 'Conqueror', 1996, 'Magnam ipsam molestias qui aliquam error pariatur eos. Sunt nisi inventore occaecati adipisci odio quaerat sunt.', 1, '2019-05-31 05:39:35', '2020-02-10 05:55:50'),
(18, 'Whirlpool', 'Specter', 1991, 'Molestias iste iure non expedita vero et. Ipsa et nobis et et enim aut facere.', 0, '2019-06-01 05:58:48', '2020-01-22 14:53:22'),
(19, 'Adventure', 'Nebula', 2013, 'Ut iure blanditiis et perspiciatis. Aut enim illo sit sit dolorem iure. Alias rerum quibusdam aut nemo ea.', 0, '2019-05-14 20:19:35', '2020-01-29 23:30:57'),
(20, 'Passion', 'Conqueror', 1995, 'Blanditiis autem quod consequatur enim optio doloribus. Iste optio facilis modi quas mollitia aut illum. Suscipit quis culpa ad dolor quis enim.', 0, '2019-06-21 00:43:14', '2020-01-24 18:20:44'),
(21, 'Reach', 'Quicksilver', 2011, 'Temporibus earum sint aut blanditiis. Consequatur alias dignissimos deleniti ipsa et quis est. Et debitis fuga et ut eligendi.', 0, '2019-08-09 13:01:49', '2020-01-14 06:43:21'),
(22, 'Alabaster', 'Conqueror', 2018, 'Amet et voluptate eum molestiae doloribus magni. Aut explicabo hic quibusdam. Dignissimos architecto error sequi qui.', 0, '2019-05-07 06:48:36', '2020-01-22 11:57:37'),
(23, 'Alabaster', 'Conqueror', 1998, 'Laboriosam non eos fugit maxime. Unde in rerum minus et illum suscipit consequatur.', 1, '2019-05-26 09:11:44', '2020-02-09 18:55:37'),
(24, 'Astral', 'Nebula', 1976, 'Quam autem et libero non. Beatae vel eos enim quibusdam cum. Et aliquam sed sint tenetur pariatur.', 1, '2019-06-23 07:06:42', '2020-02-07 00:19:47'),
(25, 'Adventure', 'Nebula', 2013, 'Qui velit quae ad accusamus. In et atque sit omnis perferendis id eos.', 0, '2019-01-29 02:19:26', '2020-01-25 02:42:47'),
(26, 'Passion', 'Conqueror', 1990, 'Possimus sint eligendi porro alias quia rerum. Repellendus neque et velit quisquam nisi. Quis officia quia culpa similique deserunt reiciendis.', 1, '2019-01-29 23:23:16', '2020-01-16 03:27:00'),
(27, 'Astral', 'Nebula', 1988, 'Nesciunt libero impedit veniam provident eos sed aspernatur. Et aut eligendi omnis non voluptates.', 0, '2019-08-15 02:39:58', '2020-01-22 08:20:21'),
(28, 'Astral', 'Nebula', 2007, 'Maxime laborum eos veritatis nesciunt voluptas quis voluptas. Rem dolor exercitationem laboriosam sed. Facere in magni animi est et.', 0, '2019-04-28 17:13:40', '2020-01-06 19:57:16'),
(29, 'Harmony', 'Specter', 1978, 'Aut dolores recusandae fuga fugit. Culpa cum voluptates soluta quae. Praesentium omnis id velit doloribus.', 0, '2019-04-12 16:23:26', '2020-01-17 07:03:55'),
(30, 'Bliss', 'Paradox', 2000, 'Fugiat a neque aut deleniti deserunt molestiae. Illum sint quia tenetur magnam. At sit sequi nulla. Eos id rerum qui dolore.', 0, '2019-11-21 23:57:36', '2020-01-17 08:03:28'),
(31, 'Bliss', 'Paradox', 1985, 'Veniam aliquam cum id qui. Quis beatae aut iure est provident. Ex quam eos vitae optio ea. Enim qui beatae beatae voluptatem ut.', 0, '2019-03-11 01:32:27', '2020-01-25 04:40:08'),
(32, 'Alabaster', 'Conqueror', 2017, 'Qui dolor eum ut qui et asperiores hic. Dignissimos porro quia vel ipsum voluptatum sit. Culpa tempore aperiam non saepe neque.', 1, '2019-11-10 16:26:24', '2020-01-30 18:04:29'),
(33, 'Astral', 'Nebula', 1999, 'Vel maxime est nisi. Magnam harum expedita et unde rerum dolorem quia eum.', 0, '2019-01-15 06:22:11', '2020-02-02 04:47:45'),
(34, 'Passion', 'Conqueror', 1984, 'Velit officiis itaque dolor omnis. Deserunt et inventore doloremque omnis. Dignissimos porro veritatis autem laboriosam vitae.', 0, '2019-06-22 21:10:07', '2020-01-13 19:08:16'),
(35, 'Blaze', 'Paradox', 2017, 'Fuga dolorum et optio reprehenderit aliquam et nihil. Et iure qui inventore non inventore. Qui corrupti non iste.', 0, '2019-08-29 23:14:31', '2020-01-16 07:16:23'),
(36, 'Astral', 'Nebula', 1999, 'Ea provident aliquam vero delectus ex deserunt dolorum. Consectetur voluptatibus dolor eveniet ut consectetur dolores nam.', 0, '2019-06-27 20:46:54', '2020-02-03 19:39:49'),
(37, 'Harmony', 'Specter', 1994, 'Est corporis ut in doloribus. Sequi et esse voluptas omnis. Corrupti nemo ipsum qui doloremque.', 0, '2019-05-09 14:05:04', '2020-02-03 22:30:28'),
(38, 'Storm', 'Quicksilver', 1989, 'Est non officiis omnis et eligendi aut unde. Quia sit sit et culpa eum tempore ea quaerat. Tempora dolorum placeat hic maiores nobis.', 1, '2019-03-02 00:32:39', '2020-01-08 22:46:26'),
(39, 'Bliss', 'Paradox', 2015, 'Impedit quis officiis maxime et modi dolore. Qui sit iusto corrupti et corrupti velit. Sed ut atque dolorem blanditiis ut sunt qui sint.', 0, '2019-03-19 06:03:46', '2020-01-03 13:45:32'),
(40, 'Harmony', 'Specter', 1976, 'Dolor quaerat veniam debitis ad ratione et odit. Ea culpa et maxime itaque incidunt assumenda. Ut itaque rerum fuga incidunt qui aut quia.', 0, '2019-06-24 11:43:36', '2020-01-01 10:02:48'),
(41, 'Adventure', 'Nebula', 1997, 'Quas quaerat iure sit debitis rerum aut. Molestiae incidunt eius minus quibusdam. Non magni non labore accusamus.', 0, '2019-10-12 16:34:02', '2020-01-07 11:53:22'),
(42, 'Astral', 'Nebula', 1985, 'Iusto praesentium libero corporis et molestiae quis. Deleniti dicta numquam cumque harum omnis aut. Sunt unde aspernatur eveniet dolor quasi.', 1, '2019-01-25 10:06:55', '2020-01-06 07:36:32'),
(43, 'Reach', 'Quicksilver', 1997, 'Suscipit nulla est ad magni omnis voluptas ullam. Unde velit porro et reiciendis. Nesciunt eos nesciunt exercitationem.', 0, '2019-05-01 01:49:24', '2020-01-10 09:31:34'),
(44, 'Alabaster', 'Conqueror', 1997, 'Ipsum deserunt recusandae molestiae ullam natus repellat. Ullam occaecati explicabo modi ea. Molestiae quia et harum.', 0, '2019-12-18 22:01:03', '2020-01-17 11:32:18'),
(45, 'Storm', 'Quicksilver', 2014, 'Consectetur quo voluptas provident cupiditate ratione. Ea recusandae consequatur eius.', 0, '2019-06-10 17:12:48', '2020-02-05 00:55:56'),
(46, 'Alabaster', 'Conqueror', 2008, 'Impedit enim non voluptatem at natus id. Corporis aut laudantium dolorum corporis non. Totam sed eligendi et corrupti totam eum nisi.', 1, '2019-11-21 01:33:38', '2020-01-18 20:08:39'),
(47, 'Alabaster', 'Conqueror', 2000, 'Sint autem in in et est. Tempora aut vel iusto sed. Consequatur nihil quas qui. Possimus quia iusto velit voluptatem eveniet iusto.', 0, '2019-06-11 20:31:53', '2020-02-01 17:55:39'),
(48, 'Astral', 'Nebula', 1987, 'Quis qui consectetur cumque odit optio. In ipsum expedita voluptates reprehenderit. Voluptates magni temporibus dolorem quaerat id.', 1, '2019-05-10 12:25:37', '2020-01-19 06:23:11'),
(49, 'Adventure', 'Nebula', 1985, 'Iste placeat neque culpa et ut aut voluptatibus. Aperiam qui eaque error soluta tenetur qui saepe. Culpa quis velit vel voluptatem.', 0, '2019-01-27 01:49:24', '2020-01-31 18:32:08'),
(50, 'Storm', 'Quicksilver', 2016, 'Aut quod aliquam consequatur veritatis corporis. Ab molestiae vel nihil voluptate sunt et modi et. Esse est in temporibus eveniet sequi est.', 1, '2019-08-24 16:31:34', '2020-01-08 22:20:11'),
(51, 'Whirlpool', 'Specter', 1989, 'Nemo esse non corrupti sed omnis aut enim nisi. Laborum et consequatur sequi. Dicta perspiciatis sed reprehenderit quo illo accusantium unde.', 1, '2019-12-19 14:03:26', '2020-01-09 09:47:31'),
(52, 'Whirlpool', 'Specter', 1989, 'Enim distinctio facere laboriosam porro ducimus. Ullam distinctio officiis nostrum fuga quis. Rerum sequi esse vel aliquid.', 1, '2019-07-23 16:23:42', '2020-01-16 15:23:50'),
(53, 'Adventure', 'Nebula', 1986, 'Libero nobis quibusdam est. Laborum harum delectus placeat repellendus minima. Adipisci illo illum ipsam sapiente occaecati quia.', 0, '2019-01-04 21:03:40', '2020-02-06 16:00:52'),
(54, 'Passion', 'Conqueror', 2008, 'Repellendus fuga voluptas nisi molestiae quas et est. A velit ea eos. Debitis perspiciatis consequatur aspernatur est veritatis.', 1, '2019-11-27 16:29:07', '2020-02-07 10:04:52'),
(55, 'Adventure', 'Nebula', 2017, 'Quae laborum et ut aut maiores. Omnis sit necessitatibus ipsa modi quidem ab. Aperiam nobis consectetur quae ut.', 0, '2019-10-31 12:56:00', '2020-01-18 22:54:29'),
(56, 'Adventure', 'Nebula', 1980, 'Consectetur est velit et. Veniam placeat velit sint aut ipsam. Dolor sed reprehenderit voluptate excepturi neque dolorem veritatis.', 1, '2019-03-22 22:55:41', '2020-01-21 15:38:22'),
(57, 'Alabaster', 'Conqueror', 1982, 'Nesciunt saepe placeat et quas vero. Dolorum rem maxime minus ut ea neque pariatur. Reiciendis facilis error maiores ut voluptatem rerum dolores.', 0, '2019-02-09 21:21:46', '2020-01-31 18:44:00'),
(58, 'Passion', 'Conqueror', 1998, 'Quas dolores nostrum fugit quisquam. Eaque consequatur ea ratione aut alias consequuntur. Et dolorem corporis aliquam magni sint et consequatur.', 0, '2019-02-01 04:10:52', '2020-01-30 15:49:51'),
(59, 'Bliss', 'Paradox', 1979, 'Commodi sit suscipit quo cumque iste. Nostrum sequi vel repudiandae blanditiis. Molestiae ullam hic sunt.', 0, '2019-11-24 22:57:07', '2020-01-26 12:10:07'),
(60, 'Bliss', 'Paradox', 1993, 'Rem eius accusantium qui sed dolorem sit. Enim doloribus facere id. Consequatur aut repudiandae officiis voluptatem.', 0, '2019-08-01 02:15:02', '2020-01-16 08:01:05'),
(61, 'Blaze', 'Paradox', 1992, 'Sunt adipisci beatae iste consequatur consequatur ex. Facilis quod itaque fugit veniam aperiam ex cum fugit. In qui soluta maiores est.', 1, '2019-04-05 05:50:24', '2020-01-15 19:34:57'),
(62, 'Astral', 'Nebula', 1992, 'Dolor consectetur quia omnis et culpa quis. Excepturi velit nulla aliquid quia expedita facilis velit. Expedita aut ut dignissimos.', 0, '2019-12-06 19:10:42', '2020-01-21 03:23:43'),
(63, 'Alabaster', 'Conqueror', 2011, 'Architecto saepe veniam ipsa et eius. Eligendi delectus architecto tenetur ea. Eos nam sint dicta doloremque eius nobis.', 1, '2019-12-02 08:50:59', '2020-01-21 22:29:04'),
(64, 'Blaze', 'Paradox', 1990, 'Enim quis debitis id qui consequatur consequatur aut. Ducimus magni quam nam ut et. Quia rerum labore molestiae eum itaque.', 1, '2019-09-10 03:42:10', '2020-01-24 05:49:29'),
(65, 'Alabaster', 'Conqueror', 1988, 'Non quis est quod soluta. Voluptatem similique qui autem cum maiores rem molestiae. Ut ratione architecto occaecati occaecati asperiores.', 0, '2019-11-27 04:54:53', '2020-02-10 17:00:41'),
(66, 'Harmony', 'Specter', 1994, 'Dignissimos ut asperiores voluptatibus et voluptatum. Molestiae dicta odio voluptas ut. Fugit non perspiciatis enim ea. Assumenda et fugiat tempora.', 0, '2019-10-02 17:17:29', '2020-01-19 11:14:34'),
(67, 'Passion', 'Conqueror', 1982, 'Illo dolore itaque maxime nam. Inventore praesentium non minus officiis ea. Maxime dolorem facilis dignissimos est nostrum dolor.', 1, '2019-12-08 02:05:39', '2020-01-19 05:00:23'),
(68, 'Harmony', 'Specter', 1983, 'Et sit quaerat eius amet. Dignissimos qui temporibus quia voluptas ea. Veniam non eos mollitia esse cumque enim. Animi doloribus sed animi alias.', 0, '2019-09-19 09:30:17', '2020-01-13 23:18:52'),
(69, 'Storm', 'Quicksilver', 2004, 'Et reprehenderit fuga eum quis illo illum eaque. Reprehenderit voluptatem vel explicabo aliquid corrupti libero.', 0, '2019-10-28 18:54:51', '2020-01-01 12:56:22'),
(70, 'Astral', 'Nebula', 2002, 'Praesentium velit quo non quo ea et. Sit molestiae est praesentium provident. Nam iste culpa omnis incidunt.', 0, '2019-05-29 04:37:59', '2020-01-09 23:26:53'),
(71, 'Harmony', 'Specter', 1987, 'Vitae explicabo molestias ut consequatur necessitatibus. Voluptas sed fugit dolore tenetur architecto ea libero.', 0, '2019-08-06 20:55:06', '2020-01-11 06:37:18'),
(72, 'Alabaster', 'Conqueror', 2009, 'Quia porro omnis et tempora. Illum veritatis vel facere sit.', 0, '2019-12-09 15:00:47', '2020-01-18 05:40:00'),
(73, 'Storm', 'Quicksilver', 1982, 'Maxime et est ea laudantium et aut. Commodi adipisci et nihil architecto officia tempore quo. Nobis natus et ea placeat hic quidem minima.', 0, '2019-09-23 00:48:45', '2020-01-26 00:07:14'),
(74, 'Adventure', 'Nebula', 2015, 'Nemo aut consequatur delectus odio neque dolor ratione. Non quisquam ex quia architecto quisquam. Est sequi illo repellendus porro quis nobis quis.', 0, '2019-04-13 01:50:13', '2020-01-12 23:16:21'),
(75, 'Passion', 'Conqueror', 1975, 'Vitae facere soluta sed quia dolorem. Quis maxime dicta sint vero et id. Nihil quae quisquam aut autem ut repellat occaecati.', 0, '2019-08-07 13:04:23', '2020-01-09 22:53:29'),
(76, 'Blaze', 'Paradox', 1992, 'Occaecati omnis tempora similique possimus. Rerum est illum culpa velit labore quibusdam. Sed ut nulla aliquam eos.', 0, '2019-08-21 14:23:24', '2020-02-05 14:30:32'),
(77, 'Alabaster', 'Conqueror', 1977, 'Autem suscipit aut excepturi. Eius facilis et aut voluptas quia eum beatae saepe. Eum possimus eligendi dolores consectetur.', 1, '2019-03-30 22:14:27', '2020-01-31 20:16:25'),
(78, 'Astral', 'Nebula', 1996, 'Esse nulla pariatur atque quam minima qui explicabo eum. Libero cumque provident cumque voluptatibus. Sed illo enim occaecati corporis est aut.', 0, '2019-09-23 07:32:59', '2020-01-20 22:35:26'),
(79, 'Blaze', 'Paradox', 1978, 'Labore ex fuga id debitis dicta. Qui a dolores occaecati ipsa dolor. Non aut sed nulla. Libero sint qui quibusdam soluta.', 0, '2019-05-01 02:22:26', '2020-01-30 11:29:28'),
(80, 'Astral', 'Nebula', 1980, 'Rerum maiores odio quis magnam deserunt. Illo quos expedita doloremque hic commodi. Sint dignissimos sapiente hic quia.', 0, '2019-03-15 02:26:23', '2020-01-10 19:47:04'),
(81, 'Adventure', 'Nebula', 1999, 'Sit ad voluptatem quo qui recusandae. Consectetur et maiores ut quam est exercitationem. Vel quia omnis non eum qui vero.', 0, '2019-01-19 21:09:20', '2020-01-29 18:47:38'),
(82, 'Blaze', 'Paradox', 1996, 'Placeat voluptate mollitia nam consectetur. Ut nulla est aliquam expedita voluptatem. Sed dolorem ut eum.', 0, '2019-03-25 20:24:17', '2020-02-03 20:44:05'),
(83, 'Alabaster', 'Conqueror', 2016, 'Et eum consequatur laudantium enim. Quisquam dolor ad est odio repellendus quo totam. Suscipit vitae voluptatibus magni.', 1, '2019-12-18 05:50:46', '2020-01-02 00:49:31'),
(84, 'Blaze', 'Paradox', 1977, 'Voluptatem rem ut explicabo ducimus. Assumenda est corrupti rem aspernatur eaque quasi. Ut odit nobis qui nostrum quidem.', 0, '2019-09-16 02:48:36', '2020-01-04 07:16:52'),
(85, 'Reach', 'Quicksilver', 1995, 'Et quidem ex perspiciatis est et nisi. Tempore eligendi ut quia ea quo tenetur. Rem culpa nemo sit quia. Aliquid qui dolor autem qui.', 1, '2019-10-20 03:03:13', '2020-01-14 05:59:52'),
(86, 'Whirlpool', 'Specter', 1982, 'Quidem veritatis quis minus ut autem et. Ex iusto quasi deleniti culpa nostrum molestiae. Et temporibus possimus aliquid sunt sint odio perferendis.', 0, '2019-04-21 14:58:40', '2020-01-15 15:20:15'),
(87, 'Storm', 'Quicksilver', 1980, 'Consequatur ut temporibus non fuga. Incidunt optio nisi quaerat sit et. Aut voluptatibus numquam enim dolorum qui id.', 0, '2019-04-28 23:14:40', '2020-02-02 21:40:36'),
(88, 'Astral', 'Nebula', 1976, 'Repellendus sequi est quam ut magnam ipsa. Accusantium earum omnis doloribus nihil aut eos odio. Qui qui vel sequi optio.', 0, '2019-12-14 00:37:17', '2020-01-08 22:18:11'),
(89, 'Bliss', 'Paradox', 2002, 'Animi commodi dolorum ut quae alias sint. Quis ducimus et suscipit facilis debitis minus et. Ut rerum nam cupiditate dolore.', 1, '2019-05-19 03:18:18', '2020-01-18 10:07:52'),
(90, 'Passion', 'Conqueror', 2016, 'Qui asperiores corporis tenetur magnam. Magnam doloribus magni delectus tenetur.', 0, '2019-06-13 15:31:01', '2020-01-19 05:26:21'),
(91, 'Alabaster', 'Conqueror', 1993, 'Quos temporibus illum excepturi consequuntur. Pariatur dicta perferendis est explicabo illo. Repellat placeat ea placeat omnis velit dolore.', 0, '2019-01-16 22:46:42', '2020-01-06 13:08:58'),
(92, 'Adventure', 'Nebula', 1977, 'Fugit nobis aperiam assumenda sunt consequatur. Ratione dolorem fugit provident omnis. Modi iure sit ex illum corrupti animi aut.', 1, '2019-03-21 05:36:17', '2020-01-13 22:33:57'),
(93, 'Bliss', 'Paradox', 2015, 'Ratione quasi temporibus et saepe et. Est laborum inventore autem molestias excepturi veritatis. Sunt et voluptas omnis voluptatem velit.', 0, '2019-06-11 21:49:48', '2020-01-08 05:34:19'),
(94, 'Adventure', 'Nebula', 2018, 'Ea nulla est consequuntur eaque temporibus ullam. Impedit voluptatem doloribus eum id rerum. Animi eum quia voluptas odio qui.', 0, '2019-12-16 05:24:33', '2020-01-10 19:08:48'),
(95, 'Adventure', 'Nebula', 2010, 'Quasi ab nesciunt nemo eveniet. Consequatur odio vel autem aut quibusdam vitae. Ducimus sint sit est sapiente doloribus sequi.', 0, '2019-05-03 20:38:36', '2020-01-26 09:12:48'),
(96, 'Astral', 'Nebula', 1991, 'Non corrupti consequatur et assumenda. Quis natus qui nisi. Ipsum et cumque tempora dolore voluptas dolorum.', 0, '2019-02-11 09:14:02', '2020-01-12 19:01:23'),
(97, 'Passion', 'Conqueror', 2000, 'At suscipit autem aut architecto. Beatae sint consequatur quis quae. Sunt ex ipsa est asperiores dolores nulla.', 1, '2019-08-02 08:18:07', '2020-01-16 06:43:40'),
(98, 'Bliss', 'Paradox', 2018, 'Officiis blanditiis et voluptatem reiciendis. Placeat est eveniet quia eum quibusdam. Sint incidunt dolorem dolor numquam.', 0, '2019-09-25 17:30:56', '2020-01-15 11:36:50'),
(99, 'Reach', 'Quicksilver', 1984, 'Similique ut nemo enim rem ut aut. Ut aliquam ut quo aut inventore aut voluptates dolorem. Consequatur dolorum omnis laboriosam facilis perferendis.', 0, '2019-02-03 03:34:03', '2020-01-28 22:11:03'),
(100, 'Whirlpool', 'Specter', 1985, 'Quis provident error ut similique. Ab ipsam eos autem dolor. Suscipit iste eum est nihil ut. Quo inventore dolorum eveniet ut.', 0, '2019-12-09 14:42:19', '2020-02-09 22:34:48'),
(110, 'teste', 'vrum vrum', 2020, 'aaa aaaaa', 1, '2020-02-11 20:19:10', '2020-02-11 20:19:10');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
